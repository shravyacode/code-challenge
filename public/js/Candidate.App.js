const app = angular.module("Candidate.App", []);
app.component("itmRoot", {
  controller: class {
    constructor() {
      this.candidates = [{
        name: "Puppies",
        votes: 10,
        percentage: 34.4
      }, {
        name: "Kittens",
        votes: 12,
        percentage: 41.3
      }, {
        name: "Gerbils",
        votes: 7,
        percentage: 24.1
      }];

    }

    /*code for voting percentage calculation*/

    onVote(candidate) {

      candidate.votes++;
      console.log(`Vote for ${candidate.name}`);

      //Calculation of total votes
      var c = 0;
      for (const [key, value] of Object.entries(this.candidates)) {
        console.log(key, value.votes);
        value.votes;
        c = c + value.votes;
      }

      var percentage = (candidate.votes / c) * 100;

      candidate.percentage = percentage;
      console.log(`Vote percentage ${percentage}`);

      //Calculating percentages
      for (const [key, value] of Object.entries(this.candidates)) {
        value.percentage = (value.votes / c) * 100;
        console.log(value.name, value.percentage);
      }

    }

    /* Adding Of new Candidates*/

    onAddCandidate(candidate) {

      var found = this.candidates.some(function(el) {
        return el.name === candidate.name;
      });

      /*checking if candidate is already present and if it is null */

      if ((!found) && (candidate.name != "")) {
        this.candidates.push({
            name: candidate.name,
            votes: 0,
            percentage: 0
          }

        )
      } else {
        alert("Please enter a valid name or name already exists");
      }
      console.log(`Added candidate ${candidate.name}`);

      candidate.name = "";

    }

    /* removing the candidate*/

    onRemoveCandidate(candidate) {

      this.candidates = this.candidates.filter(function(obj) {
        return obj.name !== candidate.name;
      });
      console.log(`Removed candidate ${candidate.name}`);
    }
  },
  template: `
    <div id="home" >
        <h1>Which candidate brings the most joy?</h1>
        <itm-results
            candidates="$ctrl.candidates">
        </itm-results>
        <itm-vote
            candidates="$ctrl.candidates"
            on-vote="$ctrl.onVote($candidate)">
        </itm-vote>

        <itm-management
            candidates="$ctrl.candidates"
            on-add="$ctrl.onAddCandidate($candidate)"
            on-remove="$ctrl.onRemoveCandidate($candidate)">
        </itm-management>
        </div>
    `
});

app.component("itmManagement", {
  bindings: {
    candidates: "<",
    onAdd: "&",
    onRemove: "&"
  },
  controller: class {
    constructor() {
      this.newCandidate = {
        name: ""
      };
    }

    submitCandidate(candidate) {
      this.onAdd({
        $candidate: candidate
      });
    }

    removeCandidate(candidate) {
      this.onRemove({
        $candidate: candidate
      });
    }
  },
  template: `
    <div id="home">
        <h2>Manage Candidates</h2>

        <h3>Add New Candidate</h3>
        <form ng-submit="$ctrl.submitCandidate($ctrl.newCandidate)" novalidate>

            <label>Candidate Name</label>
            <input type="text" ng-model="$ctrl.newCandidate.name" required>

            <button class="btn btn-common" type="submit">Add</button>
        </form>

        <h3>Remove Candidate</h3>
        <table align="center">
            <tr ng-repeat="candidate in $ctrl.candidates">
                <td>{{candidate.name}}</td>
                <td><button class="btn btn-common" type="button" ng-click="$ctrl.removeCandidate(candidate)">Remove</button></td>
            </tr>
        </table>
        </div>
    `
});

app.component("itmVote", {
  bindings: {
    candidates: "<",
    onVote: "&"
  },
  controller: class {},
  template:
    `
     <div id="home">
     <div class="container">
     <div class="row">
     <div class="col-lg-6 col-md-6 about-item left-item ">

        <p>Cast your vote!!!</p>

        <button type="button"
            ng-repeat="candidate in $ctrl.candidates"
            ng-click="$ctrl.onVote({ $candidate: candidate })">
            <span ng-bind="candidate.name"></span>
        </button>
        </div>
        <div class="col-lg-6 col-md-6 about-item left-item">

        <img  src="images/voting.png" alt="vote">
        </div>
        </div>
        </div></div>
    `
});

app.component("itmResults", {
  bindings: {
    candidates: "<"
  },
  controller: class {},
  template: `
    <div id ="home">
        <h2>Live Ordered Results</h2>
        <table align="center">
        <tr>
          <th>name</th>
          <th>votes  </th>
          <th> percentage</th>
        </tr>
        <tr ng-repeat="candidate in $ctrl.candidates | orderBy:'-percentage'">
          <td>{{candidate.name}}</td>
          <td>{{candidate.votes}}</td>
          <td>{{candidate.percentage}}</td>
        </tr>
      </table>
      <h2> Live Unordered Results</h2>
      <table align="center">
      <tr class="info">
        <th>name</th>
        <th>votes  </th>
        <th>percentage</th>
      </tr>
            <tr ng-repeat="candidate in $ctrl.candidates">
            <td>{{candidate.name}}</td>
            <td>{{candidate.votes}}</td>
            <td>{{candidate.percentage}}</td>
            </tr>
        </table>
        </div>
    `
});
